/* 
 * File:   main.c
 * Author: aturner
 *
 * Created on March 19, 2015, 7:47 AM
 */

#include <stdio.h>
#include <stdlib.h>

#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "topic.h"
#include "testharness.h"

struct message m_in;
struct topic *topics[128];


int testCreate(void);
int testPublishOneMsgAndReceiveOne(void);
int testPublishMsgNoSubscribers(void);
/*
 * 
 */
int main(int argc, char** argv) {
    
    
    int tc;
    tc = testCreate();
    if (tc > 0) {
        printf("test do_topiccreate in error");
    }
    tc = testPublishOneMsgAndReceiveOne();
    if (tc > 0) {
        printf("test testPublishOneMsgAndReceiveOn in error");
    }
    tc = testPublishMsgNoSubscribers();
    if (tc > 0) {
        printf("test testPublishOneMsgAndReceiveOn in error");
    }
    tc = testTopicRemove();
    if (tc > 0) {
        printf("test testPublishOneMsgAndReceiveOn in error");
    }
    
    
    
    
    return (EXIT_SUCCESS);
}

int testCreate() {
    m_in.m1_i1 = 0;
    m_in.m1_i2 = 0;
    m_in.m1_p1 = "topic";
    
    int i, result;
    for (i =0; i < 128; i++) {
        char str1[15];
        char * str2 = "_topic";
        sprintf(str1, "%d", i);
        
        m_in.m1_p1 = strcat(str1, str2);
        result = do_topiccreate();
        if (result > 128) {
            printf("Received error %d, for topic: %d", result, i);
            return -1;
        }
    }
    return 0;
}

int testPublishOneMsgAndReceiveOne() {
    printf("Testing the publish - retrieve function\n");
    m_in.m1_i1 = 0;  //topic 0
    
    int result, i;
    i = 0;
    char str1[15];
    char * str2 = "_topic";
    sprintf(str1, "%d", i);
    m_in.m1_p1 = strcat(str1, str2);
    result = do_topicpublisher();
    
    if (result > 0) {
        printf("Received error %d", result);
        return -1;
    }
//    result = m_in.m1_i1;
    
    m_in.m1_p1 = "0_topic";
    result = do_topicsubscriber();
    if (result > 0) {
        printf("Received do_topicsubscriber error %d", result);
        return -1;
    }
    
    m_in.m1_p1 = "message1";
    result = do_publish();
    if (result > 0) {
        printf("Received error %d", result);
        return -1;
    }
    
    m_in.m1_p1 = "no_msg";
    
    result = do_retrieve();
    if (result > 0) {
        printf("Received error %d", result);
        return -1;
    }
    printf("Message %s\n", m_in.m1_p1);
    return 0;
    
}
    
    

int testPublishMsgNoSubscribers() {
    printf("Testing no subscribers\n");
    
    int result, i = 1;
    char str1[15];
    char * str2 = "_topic";
    sprintf(str1, "%d", i);
    
    m_in.m1_i1 = 1234;  //process 1234
    m_in.m1_p1 = strcat(str1, str2);
    m_in.m1_i2 = 1;
    result = do_topicpublisher();
    if (result > 0) {
        printf("Received error on do_topicpublisher %d\n", result);
        return -1;
    }
    m_in.m1_p1 = "0_topic";
    m_in.m1_p1 = "message1";
    result = do_publish();
    if (result != NOSUBSCRIBERS) {
        printf("Received error on do_publish %d\n", result);
        return -1;
    }
    
    result = do_retrieve();
    if (result != NOTASUBSCRIBER) {
        printf("Received error %d\n", result);
        return -1;
    }
    return 0;
}

int testTopicRemove() {
    printf("Testing Topic Remove\n");
    
    int result, i = 1;
    char str1[15];
    char * str2 = "_topic";
    sprintf(str1, "%d", i);
    
    m_in.m1_i1 = 1234;  //process 1234
    m_in.m1_p1 = strcat(str1, str2);
    m_in.m1_i2 = 1;
    result = do_topicremove();
    if (result > 0) {
        printf("Received error on do_topicpublisher %d\n", result);
        return -1;
    }
    result = do_topicpublisher();
    if (result == NAMEEXISTS) {
        printf("Received error on do_publish %d\n", result);
        return -1;
    }
    if (result != 0) {
        printf("Received error %d\n", result);
        return -1;
    }
    
    return 0;
}
