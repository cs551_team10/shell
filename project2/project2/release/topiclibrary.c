#ifndef _TOPIC_LIBRARY
#define _TOPIC_LIBRARY

#include<lib.h>
#include<unistd.h>
#include<stdio.h>

//#include "testharness.h"

//char **  topiclookup()
//{
//	message m;
//	m.m_type = 1;
//	_syscall(PM_PROC_NR, TOPICLOOKUP, &m);
//}

int topiccreate(const char * arg)
{
	message m;
	m.m_type = 1;
	m.m1_i2 = strlen(arg) + 1;
	m.m1_p1 = (char *) __UNCONST(arg);
        printf("lib lib\n");
	return _syscall(PM_PROC_NR, TOPICCREATE, &m);
}


int topicpublisher(const char * name) 
{
        message m;
	m.m_type = 1;
	m.m1_i2 = strlen(name) + 1;
	m.m1_p1 = (char *) __UNCONST(name);
	return _syscall(PM_PROC_NR, TOPICPUBLISHER, &m); 
}


int topicsubscriber(const char * name) 
{
        message m;
	m.m_type = 1;
	m.m1_i2 = strlen(name) + 1;
	m.m1_p1 = (char *) __UNCONST(name);
	return _syscall(PM_PROC_NR, TOPICSUBSCRIBER, &m); 
}


int topicpublish(int topicId, const char * msg) 
{
        message m;
	m.m_type = 1;
	m.m1_i1 = topicId;
	m.m1_i2 = strlen(msg) + 1;
	m.m1_p1 = (char *) __UNCONST(msg);
	return _syscall(PM_PROC_NR, TOPICPUBLISH, &m); 
}


int topicretrieve(char * msg, int topicId) 
{
        message m;
	m.m_type = 1;
	m.m1_i1 = topicId;
	m.m1_p1 = msg;
	int error;
	error = _syscall(PM_PROC_NR, TOPICRETRIEVE, &m); 
	//strcpy(msg, m.m1_p1, 8);
	//(*msg) = (char *) m.m1_p1;
	return error;
}

#endif /* !_TOPIC_LIBRARY */
