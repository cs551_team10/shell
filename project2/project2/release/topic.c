#include <stdio.h>
#include <stdlib.h>
#include "pm.h"
#include <sys/stat.h>
#include <minix/com.h>
#include <minix/vm.h>
#include <sys/types.h>
#include <string.h>
#include <libexec.h>
#include <sys/ptrace.h>
#include "mproc.h"
#include "topic.h"
//#include "testharness.h"

topic *topics[128];
int do_topiccreate() {
    int i;
    char * name = malloc(256 * (sizeof(char)));
	int ret = sys_vircopy(mp->mp_endpoint, D, 
		(long)m_in.m1_p1, SELF, D, (long)name, m_in.m1_i2);
    for (i = 0; i < MAXTOPIC; i++) {
        if (topics[i] != NULL && (strcmp(topics[i]->name, name) == 0)) {
            return NAMEEXISTS;
        }
    }
//
    topic *t;
    for (i = 0; i < MAXTOPIC; i++) {
        if (topics[i] == NULL) {
            t = (topic *) malloc(sizeof(topic));
            t->name = name;
            t->id = i;
            t->deliveries = malloc(5 * sizeof(delivery));
            t->publishers = malloc(MAXPUBLISHERS * sizeof(int));
            t->subscribers = malloc(MAXSUBSCRIBERS * sizeof(subscriber *));
            t->nextPub = 0;
            t->nextSub = 0;
            t->nextMsg = 0;
            topics[i] = t;
            m_in.m1_i1 = i;
            return 0;
        }
    }
    return TOPICFULL;
}

int do_topicpublisher() {
    int i, pid;
    pid=mproc[who_p].mp_pid; // process id
    
    char * name = malloc(256 * (sizeof(char)));
    sys_vircopy(mp->mp_endpoint, D, 
		(long)m_in.m1_p1, SELF, D, (long)name, m_in.m1_i2);
    //
    for (i = 0; i < MAXTOPIC; i++) {
        if (topics[i] != NULL) {
            if (strcmp(topics[i]->name, name) == 0) {
                topics[i]->publishers[topics[i]->nextPub++] = pid;
    		printf("in t_publisher nextPub %d\n", topics[i]->nextPub);
    	        free(name);
                return i;
            }
        }
    }
    free(name);
    return NOTOPIC;
}

int do_topiclookup() {
    int i, pid;
    pid=mproc[who_p].mp_pid;  // process id
    topic *t;
    char ** tops;
    char ** retlist;
    
    for (i = 0; i < MAXTOPIC; i++) {
        t = topics[i];
        tops = (char **) malloc(sizeof(char**));
        retlist = tops;
        if (t != NULL) {
            tops = t->name;
        }
    }
    m_in.m1_p1 = retlist;
    
}
        

int do_topicsubscriber() {
    int i, pid, j;
    pid=mproc[who_p].mp_pid;
  printf("in do_tsubscriber pid: %d\n", pid); 

    topic *t;
    struct subscriber *s = NULL;
    char * name = malloc(256 * (sizeof(char)));
    int ret = sys_vircopy(mp->mp_endpoint, D, 
		(long)m_in.m1_p1, SELF, D, (long)name, m_in.m1_i2);
    if (ret != 0) {
   	return ERROR;
    }  
    for (i = 0; i < MAXTOPIC; i++) {
        t = topics[i];
        if (t != NULL) {
            if(strcmp(t->name, name) == 0) {
  printf("in do_tsubscriber name matched: %d\n", pid); 
                s = (struct subscriber*) malloc(sizeof(struct subscriber));
                s->id = pid;
                s->nextMsg = t->nextMsg;
  printf("in do_tsubscriber s->id is: %d\n", s->id); 
                s->eligible = malloc(5*sizeof(int));
                //topics[i]->id = pid;
                t->subscribers[t->nextSub++] = s;
                
                //check deliveries
                for (j = 0; j < 5; j++) {
                    if (t->deliveries[j].count == 0) {
                        s->eligible[j] = 0;
                    }
                }
                m_in.m1_i1 = i;
                free(name);
                return 0;
            }
        }
    }
    free(name);
    return NOTOPIC;
}

int do_retrieve() {
    int pid, i, topicId, j;
    topic *t;
    delivery *d;
    subscriber *s;
    
    pid=mproc[who_p].mp_pid;
    topicId = m_in.m1_i1;
    char * name = malloc(256 * (sizeof(char)));
    
    t = topics[topicId];
    if (t != NULL) { 
        if (t->nextSub == 0) {
  printf("in do_retrieve sub was 0? pid: %d\n", pid); 
            return NOTASUBSCRIBER;
        }
  printf("Before loop do_retrieve nextSub = %d\n", t->nextSub); 
        for (i = 0; i < t->nextSub; i++) {
  printf("in do_retrieve nextSub = %d\n", t->nextSub); 
            s =  t->subscribers[i];
  printf("in do_retrieve for loop s->id: %d\n", s->id); 
  printf("in do_retrieve for loop pid: %d\n", pid); 
                
            if (s->id  == pid) {
                //this subscriber is allowed
                for (j = 0; j < 5; j++) {
  printf("in do_retrieve j for loop j: %d\n",j); 
                    int retMsg = s->nextMsg++ % 5;
  printf("in do_retrieve j for loop ret: %d\n",retMsg); 
                    if (s->eligible[retMsg]) {
  printf("in do_retrieve before nam\n"); 
                        name = t->deliveries[retMsg].msg;
  printf("in do_retrieve after name : %s\n", name); 
                        int count = t->deliveries[retMsg].count;
                        t->deliveries[retMsg].count = --count;
                        s->eligible[retMsg] = 0;  //not eligible to receive again
                        if (count == 0) {
                            for (i = 0; i < t->nextSub; i++) {
                                s = t->subscribers[i];
                                s->eligible[retMsg] = 0;
                            }
                        }
    			int ret = sys_vircopy(SELF, D,
				(long)t->deliveries[retMsg].msg,
				mp->mp_endpoint, S, (long)m_in.m1_p1, strlen(name) +1);
                        free(t->deliveries[retMsg].msg);
                        return ret;

                    }
                }
           } 
           //return NOTASUBSCRIBER;
        }
        return NOMESSAGES;
    }
            
    return NOTOPIC;
}
    
int do_publish() {
    int i, ret, pid, nextMsg, topicId;
    topic *t;
    t = topics[0];
    printf("TEST \n");
    for (i = 0; i < 5; i++) {
        int nextMsg = t->nextMsg++ % 5;
printf("something %s\n", t->deliveries[nextMsg].msg);
}    
    pid=mproc[who_p].mp_pid;

    topicId = m_in.m1_i1;  // topic id
    
    t = topics[topicId];
    if (t == NULL) {
        return NOTOPIC;
    }
    if (t->nextSub == 0) {
        //message will never be delivered
        return NOSUBSCRIBERS;
    }
    for (i = 0; i < 5; i++) {
    		printf("in t_publish nextmsg %d\n", t->nextMsg);
        nextMsg = t->nextMsg++ % 5;
    		printf("in t_publish nextmsg %d\n", t->nextMsg);
        if (t->deliveries[nextMsg].count == 0) {
            //# of subscribers
            t->deliveries[nextMsg].count = t->nextSub;
            t->deliveries[nextMsg].msg = malloc(256*sizeof(char));
    	    ret = sys_vircopy(mp->mp_endpoint, D, 
		(long)m_in.m1_p1, SELF, D, (long)t->deliveries[nextMsg].msg, m_in.m1_i2);
  printf("return %d\n", ret); 
printf("something %s\n", t->deliveries[nextMsg].msg);
            for (i = 0; i < t->nextSub; i++) {
                t->subscribers[i]->eligible[nextMsg] = 1;
            }
            return 0;
        }
    }
    return TOPICFULL;
}

