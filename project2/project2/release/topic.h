/* 
 * File:   topic.h
 * Author: aturner
 *
 * Created on March 19, 2015, 7:48 AM
 */

#ifndef TOPIC_H
#define	TOPIC_H



#include<lib.h>

#define MAXTOPIC 128
#define MAXSUBSCRIBERS 128
#define MAXPUBLISHERS 128
#define NOTOPIC 201 
#define NAMEEXISTS 202
#define TOPICFULL 203
#define NOTAPUBLISHER 204
#define NOTASUBSCRIBER 205
#define NOMESSAGES 206
#define NOSUBSCRIBERS 207
#define ERROR 208

typedef struct delivery{
    char * msg;
    int count;  // number to be delivered
} delivery;


typedef struct topic {
	int id;			/* Unique ID  */
	char * name; 		/* Topic name */
	int *publishers;		
	struct subscriber ** subscribers;		
	int nextPub;
	int nextSub;
	int nextMsg;
        struct delivery *deliveries;		
} topic;

extern struct topic *topics[MAXTOPIC];


typedef struct publisher
{
	int id;
	int nextmsg;
        
} publisher;

typedef struct subscriber
{
	int id;
	int *eligible;
        int nextMsg;
}subscriber;





//struct lookup IGLookup()
//{
//	message m;
//	m.m_type = 1;
//	struct lookup info;
//	_syscall(PM_PROC_NR, IGLOOKUP, &m);
//	info.i1 = m.m7_i1;
//	info.i2 = m.m7_i2;
//	info.i3 = m.m7_i3;
//	info.i4 = m.m7_i4;
//	info.i5 = m.m7_i5;
//	return info;
//}
#endif	/* TOPIC_H */

