/* 
 * File:   testharness.h
 * Author: aturner
 *
 * Created on March 20, 2015, 11:12 AM
 */

#ifndef TESTHARNESS_H
#define	TESTHARNESS_H


typedef struct message {
    int m1_i1;
    int m1_i2;
    char * m1_p1;
}message;

extern struct message m_in;

#endif	/* TESTHARNESS_H */

