#include <stdio.h>
#include <stdlib.h>

#include "topic.h"
#include "testharness.h"

int do_topiccreate() {
    int i;
    char * name = malloc(256 * (sizeof(char)));
    strcpy(name, m_in.m1_p1);

    for (i = 0; i < MAXTOPIC; i++) {
        if (topics[i] != NULL && (strcmp(topics[i]->name, name) == 0)) {
            return NAMEEXISTS;
        }
    }
//
    topic *t;
    for (i = 0; i < MAXTOPIC; i++) {
        if (topics[i] == NULL) {
            t = (topic *) malloc(sizeof(topic));
            t->name = name;
            t->id = i;
            t->deliveries = malloc(5 * sizeof(delivery));
            t->publishers = malloc(MAXPUBLISHERS * sizeof(int));
            t->subscribers = malloc(MAXSUBSCRIBERS * sizeof(subscriber *));
            t->nextPub = 0;
            t->nextSub = 0;
            t->nextMsg = 0;
            topics[i] = t;
            m_in.m1_i1 = i;
            return 0;
        }
    }
    return TOPICFULL;
}

int do_topicpublisher() {
    int i, id;
    id = m_in.m1_i1;  // process id
    
    for (i = 0; i < MAXTOPIC; i++) {
        if (topics[i] != NULL) {
            if (strcmp(topics[i]->name, m_in.m1_p1) == 0)
                topics[i]->publishers[topics[i]->nextPub++] = id;
                return i;
        }
    }
    return NOTOPIC;
}

int do_topiclookup() {
    int i, id, j;
    id = m_in.m1_i1;  // process id
    topic *t;
    subscriber *s;
    char ** tops;
    char ** retlist;
    
    for (i = 0; i < MAXTOPIC; i++) {
        t = topics[i];
        tops = (char **) malloc(sizeof(char**));
        retlist = tops;
        if (t != NULL) {
            tops = t->name;
        }
    }
    m_in.m1_p1 = retlist;
    
}
        

int do_topicsubscriber() {
    int i, id, j;
    id = m_in.m1_i1;  // process id
    topic *t;
    struct subscriber *s = NULL;
    
    for (i = 0; i < MAXTOPIC; i++) {
        t = topics[i];
        if (t != NULL) {
            if(strcmp(t->name, m_in.m1_p1) == 0) {
                s = (struct subscriber*) malloc(sizeof(struct subscriber));
                s->id = 0;
                s->eligible = malloc(5*sizeof(int));
                topics[i]->id = id;
                t->subscribers[t->nextSub++] = s;
                
                //check deliveries
                for (j = 0; j < 5; j++) {
                    if (t->deliveries[j].count == 0) {
                        s->eligible[j] = 1;
                    }
                }
                m_in.m1_i1 = i;
                return 0;
            }
        }
    }
    return NOTOPIC;
}

//
//
//int do_topicpublish() {
//    int pid, i, topicId, j;
//    topic *t;
//    delivery *d;
//    char * msg;
//    
//    pid = m_in.m1_i1;
//    topicId = m_in.m1_i2;
//    
//    t = topics[topicId];
//    if (t != NULL) {
//        for (i = 0; i < t->nextPub; i++) {
//            if (t->publishers[i] == pid) {
//                //can publish
//                
//                for (i = 0; i < 5; i++) {
//                    d = t->deliveries[i];
//                    for (j = 0; j < d->mark; j++) {
//                        if (pid != d->nodeliver[j]) {
//                            msg = d->msg;
//                        }
//                        //don't deliver
//                    }
//                }
//            }
//                
//        }
//    }
//    
//}

int do_retrieve() {
    int pid, i, topicId, j;
    topic *t;
    delivery *d;
    subscriber *s;
    char * msg;
    
    pid = m_in.m1_i1;
    topicId = m_in.m1_i2;
    
    t = topics[topicId];
    if (t != NULL) { 
        if (t->nextSub == 0) {
            return NOTASUBSCRIBER;
        }
        for (i = 0; i < t->nextSub; i++) {
            s =  t->subscribers[i];
                
            if (s->id  == pid) {
                //this subscriber is allowed
                for (j = 0; j < 5; j++) {
                    int retMsg = s->nextMsg++ % 5;
                    if (s->eligible[retMsg]) {
                        m_in.m1_p1 = t->deliveries[retMsg].msg;
                        int count = t->deliveries[retMsg].count;
                        t->deliveries[retMsg].count = --count;
                        
                        if (count == 0) {
                            for (i = 0; i < t->nextSub; i++) {
                                s = t->subscribers[i];
                                s->eligible[retMsg] = 1;
                            }
                        }
                        return 0;

                    }
                }
            }
            return NOTASUBSCRIBER;
        }
        return NOMESSAGES;
    }
            
    return NOTOPIC;
}
    
int do_publish() {
    int i, pid, topicId;
    pid = m_in.m1_i1;  // process id
    topicId = m_in.m1_i2;  // topic id
    topic *t;
    
    t = topics[topicId];
    if (t == NULL) {
        return NOTOPIC;
    }
    if (t->nextSub == 0) {
        //message will never be delivered
        return NOSUBSCRIBERS;
    }
    for (i = 0; i < 5; i++) {
        int nextMsg = t->nextMsg++ % 5;
        if (t->deliveries[nextMsg].count == 0) {
            //# of subscribers
            t->deliveries[nextMsg].count = t->nextSub;
            t->deliveries[nextMsg].msg = m_in.m1_p1;
            for (i = 0; i < t->nextSub; i++) {
                t->subscribers[i]->eligible[nextMsg] = 1;
            }
            return 0;
        }
    }
    return TOPICFULL;
}

int do_topicremove() {
    int i,j;
    subscriber *s;
    char * name = malloc(256 * (sizeof(char)));
    strcpy(name, m_in.m1_p1);
    
    for (i = 0; i < MAXTOPIC; i++) {
        if (topics[i] != NULL && (strcmp(topics[i]->name, name) == 0)) {
            for (j = 0; j < 5; j++) {
                free(topics[i]->deliveries[j].msg);
            }
            for (j = 0; j < topics[i]->nextSub; j++) {
                s = topics[i]->subscribers[j];
                free(s->eligible);
                free(s);
            }
            free(topics[i]->publishers);
            free(topics[i]->name);
        }
    }
//
    return 0;
}            
