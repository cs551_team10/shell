#include "calc.c"

int main()
{
	fflush(stdin);
	
	int selection, x, y, result;

	printf("Simulation of Simple Calculator\n");
	printf("Enter you numbers:\n");
	
	printf("x: ");
	scanf("%d", &x);
	
	printf("y: ");
	scanf("%d", &y);
	
	printf("Please select your operation: \n");
	scanf("%d", &selection);
	
	switch(selection) {
		
		case 1:
		printf("x + y = %d\n", addition(x, y));
		break;
		
		case 2:
		printf("x - y = %d\n", subtraction(x, y));
		break;
		
		case 3:
		printf("x * y = %d\n", multiplication(x, y));
		break;
		
		case 4:
		printf("x / y = %d\n", division(x, y));
		break;
		
		default:
		printf("Error!");
		break;
	}
return 0;
}