#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "parseCommand.h"



void 
parseCommands(char ** primaryCommands, char ** commands, char ** args, const char * str ) {
    static char encodedString[128];
    encodedString[0] = '\0';
    char c;

    char * testString = malloc(256 * (sizeof(char)));
    strcpy(testString, str);
    
    
    int i=0;    
    primaryCommands[0] = malloc(sizeof(char*));
    commands[0] = malloc(sizeof(char*));
    primaryCommands[0] = NULL;
    commands[0] = NULL;
    while(1) {
        c = *(testString++);
        if (i == 0 && c == '\0') {
            return;
        }
        i++;
        if (c == (char) '(') {
            c = *(testString);
            //command started
            break;
        }
        if (c == '\0') {
            fprintf(stderr, "commands not entered properly, must be enclosed in ()\n");
            return;
        }
    }

    char * cmdTok;
    char * cmdOption;
    char *regex = ",";

    int regIndex = 0;
    int primIndex=0;
    char* save;
    char* newCmd;
    char* newOpt;
    char* saveOpt;
    i =0;
    
    if ((cmdTok = strtok(testString, regex)) != NULL) {
        newCmd = malloc( sizeof(char) * (20) );
        while (isspace(*cmdTok)) {
           cmdTok++;
        }
        
        if (c == '(') {
            cmdTok++;
            while (isspace(*cmdTok)) {
               cmdTok++;
            }
            //primary command
            save = newCmd;
            saveOpt = NULL;
            while ((c = *(cmdTok++)) != ')' && c != ' ' && c != '\0') {
                *(newCmd++) = c;
            }
            if (c == ' ') {
                while (isspace(*cmdTok)) {
                   cmdTok++;
                }
                cmdOption = malloc(20 * sizeof(char));
                saveOpt = cmdOption;
    
                //found command options
                while ((c = *(cmdTok++)) != ')' && c != '\0') {
                    *(cmdOption++) = c;
                }
                *cmdOption = '\0';
            }
            *newCmd = '\0';
             
            
            primaryCommands[primIndex] = malloc(sizeof(char*));
            primaryCommands[primIndex++] = save;
            args[i] = malloc(sizeof(char*));
            args[i++] = saveOpt;
        }
        else {
            save = newCmd;
            saveOpt = NULL;
            while ((c = *(cmdTok++)) != ' ' && c != ')' && c != '\0') {
                *(newCmd++) = c;
            }
            if (c == ' ') {
                cmdOption = malloc(20 * sizeof(char*));
                saveOpt = cmdOption;
                //found command options
                while ((c = *(cmdTok++)) != ')' && c != '\0') {
                    *(cmdOption++) = c;
                }
                *cmdOption = '\0';
            }
            *newCmd = '\0';
            commands[regIndex] = malloc(sizeof(char*));
            commands[regIndex++] = save;
            args[i] = malloc(sizeof(char*));
            args[i++] = saveOpt;
        }
    }
        
    while (( cmdTok  = strtok(NULL, regex)) != NULL) {
        newCmd = malloc( sizeof(char) * (20) );

       
        while (isspace(*cmdTok)) {
           cmdTok++;
        }
            
        c = *(cmdTok++);
        
        if (c ==  '(') {
            //primary command
            saveOpt = NULL;
            save = newCmd;
            while ((c = *(cmdTok++)) != ')' && c != ' ' && c != '\0' ) {
                *(newCmd++) = c;
            }
            if (c == ' ') {

                while (isspace(*cmdTok)) {
                   cmdTok++;
                }

                cmdOption = malloc(20* sizeof(char));
                saveOpt = cmdOption;
                //found command options
                while ((c = *(cmdTok++)) != ')') {
                    *(cmdOption++) = c;
                }
                *cmdOption = '\0';

            }
            *newCmd = '\0';
                
            
            primaryCommands[primIndex] = malloc(sizeof(char*));
            primaryCommands[primIndex++] = save;
            args[i] = malloc(sizeof(char*));
            args[i++] = saveOpt;
        }
        else {
            saveOpt = NULL;
            save = newCmd;
            *(newCmd++) = c;
            while ((c = *(cmdTok++)) != ' ' && c != ')' && c != '\0') {
                *(newCmd++) = c;
            }
            if (c == ' ') {
                
                while (isspace(*cmdTok)) {
                   cmdTok++;
                }
                cmdOption = malloc(20 * sizeof(char));
                saveOpt = cmdOption;
                //found command options
                while ((c = *(cmdTok++)) != ')' && c != '\0') {
                    *(cmdOption++) = c;
                }
                *cmdOption = '\0';
            }
            *newCmd = '\0';
            
            commands[regIndex] = malloc(sizeof(char*));
            commands[regIndex++] = save;
            args[i] = malloc(sizeof(char*));
            args[i++] = saveOpt;
        }
    }
    primaryCommands[primIndex] = NULL;
    commands[regIndex] = NULL;
    args[i] = NULL;
    
        
}