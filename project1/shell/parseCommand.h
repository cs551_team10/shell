/* 
 * File:   parseCommand.h
 * Author: aturner
 *
 * Created on February 12, 2015, 5:01 PM
 */

#ifndef PARSECOMMAND_H
#define	PARSECOMMAND_H

void 
parseCommands(char **, char **, char **, const char *);

#ifdef	__cplusplus
extern "C" {
#endif





#ifdef	__cplusplus
}
#endif

#endif	/* PARSECOMMAND_H */

