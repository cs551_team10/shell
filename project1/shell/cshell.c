#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include "parseCommand.h"
#include "header.c"

#include <fcntl.h>
#define CMDLENGTH 300
#define MAXPIPES 30

int pid;
char * currentDirectory;

void sigint_handler(int signo) {

    char c;
    printf("\nAre you sure? (Y/N): ");
    c=getchar();

    if(c=='Y' || c == 'y')
    {
        exit(0);
    }
}


int findCommand(char * dirname, char * command) {

	DIR * dir = opendir(dirname);

	if (!dir) {
            fprintf(stderr, "Error: %s: %s\n", dirname, strerror(errno));
	}

	while (1) {
            struct dirent * entry;
            entry = readdir(dir);

            if (!entry) {
                    break;
            }

            if (!strcmp(command, entry->d_name)) {
                    return 1;
            }
    }
    return 0;
}


void shellPrompt()
{
        printf("%s :> ",getcwd(currentDirectory, 1024));
}

int changeDir(char *directory)
{
	int i;
	i=chdir(directory);
	return i;
}

char * 
getCommands() {

    char * str = malloc(sizeof(char*) * 256);
    if (str == 0) {
        fprintf(stderr, "memory allocation failed\n");
        exit(1);
    }
    fgets (str, 256, stdin);
    /* Remove trailing newline, if there. */
    if ((strlen(str)>0) && (str[strlen (str) - 1] == '\n'))
        str[strlen (str) - 1] = '\0';

    return str;
}

int getPath(char ** home, char str[]) {
	char* path[20];
	FILE *fp;
	char line[1024];
	int num = 0, i;

	/* Open profile file to get environment variable */
	fp = fopen(".customprofile", "r");
	if (fp != NULL) {
		while (fgets(line, sizeof(line), fp) != NULL) {
			path[num] = (char*) malloc(sizeof(line));
			strcpy(path[num], line);
			num++;
		}

	} else {
		/* File is empty or illegal characters */
		printf("It is not a valid file\n");
		return -1;
	}
        
        char * pathArg;
	for (i = 0; i < num; i++) {
            if ((strstr(path[i], str)) != NULL) {

                char * save;
                char * newPath;
                char * token;
                char * paths;
                char * regex = "=";
                char c;

                if ((token = (strtok(path[i], regex))) != NULL) {
                    //this is the match
                }
                
                while ((token = (strtok(NULL, regex))) != NULL) {
                    //this is paths
                    pathArg = token;
                }

                regex = ";";
                if ((token = (strtok(pathArg, regex))) != NULL) {
                    newPath = malloc( sizeof(char) * (100) );
                    save = newPath;

                    while ((c = *(token++)) != '\0') {
                        *(newPath++) = c;
                    }
                    home[i] = malloc(sizeof(char*));
                    home[i++] = save;
                }

                while ((token = (strtok(NULL, regex))) != NULL) {
                    newPath = malloc( sizeof(char) * (100) );
                    save = newPath;

                    while ((c = *(token++)) != '\0') {
                        *(newPath++) = c;
                    }
                    home[i] = malloc(sizeof(char*));
                    home[i++] = save;
                }

                home[i] = NULL;
            }
        }
	/* Close profile file */
	fclose(fp);
	return 0;
    }
    
    
    
    
    int getValue(char* home, char str[]) {
	char* path[10];
	FILE *fp;
	char line[1024];
	int num = 0, i, k = 0, chk = 0;

	/* Open profile file to get environment variable */
	fp = fopen(".customprofile", "r");
	if (fp != NULL) {
		while (fgets(line, sizeof(line), fp) != NULL) {
			path[num] = (char*) malloc(sizeof(line));
			strcpy(path[num], line);
			num++;
		}

	} else {
		/* File is empty or illegal characters */
		printf("It is not a valid file\n");
		return -1;
	}

	int len = 0;

	for (i = 0; i < num; i++) {
		//printf("line(%d): %s\n", i, path[i]);

		if ((strstr(path[i], str)) != NULL) {
			len = strlen(path[i]);
			int j;
			for (j = 0; j < len; j++) {
				if (chk == 0) {
                                    if (*(path[i] + j) == '=') {
                                            chk = 1;
                                            continue;
                                    } else
                                            continue;
				}
				if (chk == 1) {
					*(home + k++) = *(path[i] + j);
				}
			}
		}
	}
	for (i=0; i<=strlen(home); i++){
		if (*(home+i)=='\n')
			*(home+i)='\0';
		}

	/* Close profile file */
	fclose(fp);
	return 0;
}

int main(int argc, char *argv[]) {

	int currentPipe, status, lastresult = 0, alarmActive, ifEnabled;

	char *userinput, *command, *temp;

	char *usrpath, *home;

	char bufferinput[CMDLENGTH], buffer[CMDLENGTH];


	signal(SIGINT, sigint_handler);
	

	// Read the settings of the profile and store them
	// Read file line by line
	usrpath = malloc(sizeof(char*));
	home = malloc(sizeof(char*));

        char * dirlist[20];
	getPath(dirlist, "CPATH=");
        

	getValue(home, "CHOME=");
	if (home[strlen(home) - 1] == '\n') {
		home[strlen(home) - 1] = '\0';
	}
	if ((chdir(home)) < 0) {
		fprintf(stderr, "home: '%s'\n", home);
		fprintf(stderr, "Error: chdir: %s\n", strerror(errno));
	}

        char * commands[20];
        char * primaryCommands[20];
        char * argument[20];
        char * str;


	while (1) {
            shellPrompt();
            char* cmdLine = getCommands();

            parseCommands(primaryCommands, commands, argument, cmdLine);

    
            if (commands[0] == NULL && primaryCommands[0] == NULL) {
                continue;
            }

		// if "exit", exit

            if (commands[0] != NULL) {
                if (!strncmp(commands[0], "exit", 4)) {
                        exit(0);
                }
            }
            if (primaryCommands[0] != NULL) {
                if (!strncmp(primaryCommands[0], "exit", 4)) {
                        exit(0);
                }
            }



		int pipes[MAXPIPES][2];

		int pipeCount;
		int regCommands;
		int primCmds;

                int index = 0;
                while (commands[index++] != NULL) {}
                regCommands = index -1;
                index = 0;
                while (primaryCommands[index++] != NULL) {}
                primCmds = index -1;
                
                pipeCount = regCommands + primCmds -1;
                

		if (pipeCount > 0) {
			int i = 0;
			for (i = 0; i < pipeCount; i++) {
				pipe(pipes[i]); 
			}
		}
                
                
                int commandIndex = 0;
                for (currentPipe = 0; currentPipe < pipeCount + 1; ++currentPipe) {

                    if (pipeCount >= 0) {
                                if ((command = (primaryCommands[commandIndex++])) == NULL) {
                                    primaryCommands[commandIndex] = NULL;
                                    command = commands[currentPipe-primCmds];
                                }


                        } 
                        else {
                                command = userinput;
                        }
                    
                    
                    if (strcmp(command, "cd") == 0) {
                        changeDir(argument[currentPipe]);
                        break;
                    }

                        // FORK()
                        if ((pid = fork()) < 0) {
                                fprintf(stderr, "ERROR: fork\n");
                                exit(-1);
                        }

                        // If Parent:

                        if (pid > 0) {
                            // Close pipes

                            if (pipeCount > 0) {
                                if (currentPipe == 0) {
                                    // First program 
                                    close(pipes[currentPipe][1]);
                                } else if (currentPipe == pipeCount) {
                                    // Last program 
                                    close(pipes[currentPipe - 1][0]);
                                    for (currentPipe = 0; currentPipe < pipeCount + 1;
                                            ++currentPipe) {

                                            //fprintf(stdout, "Waiting for child %i\n",currentPipe);
                                            wait(&status);
                                            lastresult = WEXITSTATUS(status);

                                    }
                                } else {
                                    // Middle pipes
                                        close(pipes[currentPipe - 1][0]);
                                        close(pipes[currentPipe][1]);

                                }
                                } else {

                                    // We don't wait if there are pipes around
                                    // Wait until child is dead

                                    waitpid(pid, &status, 0);
                                    lastresult = WEXITSTATUS(status);
                                }
                        }
                        else {
                            //child


                            if (pipeCount > 0) {
                                if (currentPipe == 0) {
                                    close(1);
                                    dup(pipes[currentPipe][1]);
                                    close(pipes[currentPipe][0]);
                                    close(pipes[currentPipe][1]);
                                } else if (currentPipe == pipeCount) {
                                    close(0);
                                    dup(pipes[currentPipe - 1][0]);
                                    close(pipes[currentPipe - 1][1]);
                                    close(pipes[currentPipe - 1][0]);
                                } else {
                                    close(1);
                                    dup(pipes[currentPipe][1]);
                                    close(0);
                                    dup(pipes[currentPipe - 1][0]);
                                    close(pipes[currentPipe][1]);
                                    close(pipes[currentPipe][0]);
                                    close(pipes[currentPipe - 1][0]);
                                }

                        }


                    int i = 0;
                    
                    

                    while (dirlist[i] != NULL) {

                            if ((findCommand(dirlist[i], command)) != 0) {
                                    break;
                            }
                            i++;
                    }

                    if (dirlist[i] == NULL) {
                            fprintf(stderr, "Error: %s: Command not found\n", command); // It fails if it doesn't find the binary in the PATHs
                            exit(-1);
                    }

                    //build exe command
                    strcpy(buffer, dirlist[i]);
                    strcat(buffer, "/");
                    strcat(buffer, command);

                    // Execute the command

                    if ((argument[currentPipe]) == NULL) {
                            execl(buffer, command, NULL, NULL);

                    } else {
                        char * argArr [3];
                        argArr[0] = buffer;
                        argArr[1] = argument[currentPipe];
                        argArr[2] = NULL;
                        execv(buffer, argArr);
                    }



                    fprintf(stderr, "Failed to run: %s\n", command);

                    exit(-1); //something went wrong, could not execl

                    }

            }
                int i = 0;
                while(commands[i++] != NULL) {
                    free(commands[i-1]);
                }
                i =0;
                while(primaryCommands[i++] != NULL) {
                    free(primaryCommands[i-1]);
                }
                i =0;
                while(argument[i++] != NULL) {
                    free(argument[i-1]);
                }
                
                
                
                
	}
        
        
	exit(-1);
}

int calc()
{
    fflush(stdin);
    
    int selection, x, y, result;

    printf("Simulation of Simple Calculator\n");
    printf("Enter you numbers:\n");
    
    printf("x: ");
    scanf("%d", &x);
    
    printf("y: ");
    scanf("%d", &y);
    
    printf("Please select your operation: \n");
    scanf("%d", &selection);
    
    switch(selection) {
        
        case 1:
        printf("x + y = %d\n", addition(x, y));
        break;
        
        case 2:
        printf("x - y = %d\n", subtraction(x, y));
        break;
        
        case 3:
        printf("x * y = %d\n", multiplication(x, y));
        break;
        
        case 4:
        printf("x / y = %d\n", division(x, y));
        break;
        
        default:
        printf("Error!");
        break;
    }
return 0;
}

int addition(int x, int y)
{
    int result = x+y;
    write_to_file(x, y, result);
    return result;
}
int subtraction(int x, int y)
{
    int result = x-y;
    write_to_file(x, y, result);
    return result;
}
int multiplication(int x, int y)
{
    int result = x*y;
    write_to_file(x, y, result);
    return result;
}
int division(int x, int y)
{
    int result = x/y;
    write_to_file(x, y, result);
    return result;
}
void write_to_file(int x, int y, int result)
{
    int counter;
    FILE *ptr_myfile;
    struct rec my_record;


    ptr_myfile = fopen("test.bin","wb");
    
    if (!ptr_myfile)
    {
        printf("Unable to open file!\n");
    }
    for (counter=1; counter<=1; counter++)
    {
        my_record.x = x;
        my_record.y = y;
        my_record.result = result;
        fwrite(&my_record, sizeof(struct rec), 1, ptr_myfile);
            
    }
    fclose(ptr_myfile);
}

int readcalc(char str[10]){
    //char str[10];
    int idxToDel = 0;

  //  printf("Enter value to read: ");
    //scanf("%s", str);

    if (strstr(str, "$"))
    {

        printf("yes\n");

        memmove(&str[idxToDel], &str[idxToDel + 1], strlen(str) - idxToDel);

        printf("String now is: %s \n", str);

        if(strstr(str, "x"))
        {
            readx();
            return 0;
        }
        else if(strstr(str,"y"))
        {
            ready();
            return 0;
        }
        else if(strstr(str,"result"))
        {
            readresult();
            return 0;
        }
        else
        {
            printf("You have entered a record that does not exist\n");
            return 0;
        }

        return 0;
    }
    else
    {
        printf("Error, please re-enter\n");
        return 0;

    }
    
    return 0;
}

void readx()
{
    int counter;
    FILE *ptr_myfile;
    struct rec my_record;
    
    ptr_myfile=fopen("test.bin","rb");

    if (!ptr_myfile)
    {
        printf("Unable to open file!\n");
    }
    fseek(ptr_myfile,sizeof(struct rec),SEEK_END);
    rewind(ptr_myfile);
    
    for (counter=1; counter>=1;counter--)
    {
        fread(&my_record, sizeof(struct rec), 1, ptr_myfile);
        printf("%d\n", my_record.x);
    }
    fclose(ptr_myfile);
}

void ready()
{
    int counter;
    FILE *ptr_myfile;
    struct rec my_record;

    ptr_myfile=fopen("test.bin","rb");

    if (!ptr_myfile)
    {
        printf("Unable to open file!\n");
    }
    fseek(ptr_myfile,sizeof(struct rec),SEEK_END);
    rewind(ptr_myfile);
    
    for (counter=1; counter>=1;counter--)
    {
        fread(&my_record, sizeof(struct rec), 1, ptr_myfile);
        printf("%d\n", my_record.y);
    }
    fclose(ptr_myfile);
}

void readresult()
{
    int counter;
    FILE *ptr_myfile;
    struct rec my_record;

    ptr_myfile=fopen("test.bin","rb");

    if (!ptr_myfile)
    {
        printf("Unable to open file!\n");
    }
    fseek(ptr_myfile,sizeof(struct rec),SEEK_END);
    rewind(ptr_myfile);

    for (counter=1; counter>=1;counter--)
    {
        fread(&my_record, sizeof(struct rec), 1, ptr_myfile);
        printf("%d\n", my_record.result);
    }
    fclose(ptr_myfile);
}