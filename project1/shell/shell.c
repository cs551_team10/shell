#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include "parse.h"   //include declarations for parse-related structs

enum
BUILTIN_COMMANDS { NO_SUCH_BUILTIN=0, EXIT,JOBS};
 
char *
buildPrompt()
{
  return  "%";
}
 
int
isBuiltInCommand(char * cmd){
  
  if ( strcmp(cmd, "exit") == 0){
    return EXIT;
  }
  return NO_SUCH_BUILTIN;
}

char * 
getCommands() {
    char buffer[256];
    char *str= buffer;
    
    fgets (str, 256, stdin);

    /* Remove trailing newline, if there. */
    if ((strlen(str)>0) && (str[strlen (str) - 1] == '\n'))
        str[strlen (str) - 1] = '\0';

//    scanf("%256[^\n]%*c", str);
//    scanf ("%s", str);
    return str;
    
}

char * command[4];
char * currentDirectory;
char * homeDir = "/home/aturner";
void shellPrompt()
{
        printf("%s :> ",getcwd(currentDirectory, 1024));
}
int 
main (int argc, char **argv)
{

    chdir(homeDir);
  
  char * cmdLine;
  parseInfo *info; //info stores all the information returned by parser.
  struct commandType *com; //com stores command name and Arg list for one command.
  fprintf(stderr, "Until you fix the exit command press ctrl-c to exit\n");

  while(1){
    shellPrompt();
    char* Commands = getCommands();
    printf("%s\n", Commands);
    if (strcmp(Commands, "") == 0) {
        continue;
    }
    if (strcmp(Commands, "exit") == 0) {
        break;
    }
    
    
      
    if (strcmp(Commands, "ls") == 0) {
        printf("In strcmp\n");
        
        
        command[0] = "ls";
        execve("/bin/ls", command, NULL);
    }
      
    //insert your code to print prompt here.
    //  fprintf(stdout, "\%\n");
    fprintf(stdout, "This is the UNIX version\n");


    if (cmdLine == NULL) {
      fprintf(stderr, "Unable to read command\n");
      continue;
    }

    //insert your code about history and !x !-x here

    //calls the parser
    info = parse(argv[1]);
    if (info == NULL){
        printf("%s\n", "info was null");
      free(cmdLine);
      continue;
    }
    //prints the info struct
    print_info(info);

    //com contains the info. of the command before the first "|"
/*
    com=&info->CommArray[0];
    if ((com == NULL)  || (com->command == NULL)) {
      free_info(info);
      free(cmdLine);
      continue;
    }
*/
    //com->command tells the command name of com
/*
    if (isBuiltInCommand(com->command) == EXIT){
      exit(1);
    }
*/
    //insert your code here.

    free_info(info);
//    free(cmdLine);
  }/* while(1) */
}
  





